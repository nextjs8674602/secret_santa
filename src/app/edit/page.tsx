
import getCurrentUser from "@/app/actions/getCurrentUser";
import { redirect } from "next/navigation"
import GroupProfile from "@/app/components/GroupProfile";
import Navbar from "@/app/components/Navbar";
import Sidebar from "@/app/components/Sidebar";
import EditProfile from "../components/EditProfile";


const Edit: React.FC = async () => {
  const currentUser = await getCurrentUser()
  

  return (
    <div className="min-h-full">
      <Navbar currentUser={currentUser} />
      <div className="pt-20 flex justify-between min-h-full">
        <div className="w-full mx-auto md:w-3/5">
          <EditProfile currentUser={currentUser} />
        </div>
        
      </div>
    </div>
  );
};

export default Edit;
