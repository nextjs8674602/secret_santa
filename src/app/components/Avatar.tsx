'use client';

import Image from 'next/image'

interface AvatarProps {
    src: string | null | undefined,
    height:number,
    width:number
}

const Avatar: React.FC<AvatarProps> = ({height,width}) => {
    return (
        <Image
         className="rounded-full"
         height={height}
         width={width}
         alt="Avatar"
         src={"/placeholder.jpg"}
         />
    )
}

export default Avatar;