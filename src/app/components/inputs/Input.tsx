import { useCallback, useState } from "react";
import { FieldValues, UseFormRegister } from "react-hook-form";
import { FaRegEye } from "react-icons/fa";
import { FaRegEyeSlash } from "react-icons/fa";




interface InputProps {
  label: string;
  type: string;
  id: string;
  status?: any;
  register: UseFormRegister<FieldValues>;
}

const Input: React.FC<InputProps> = ({
  label,
  type,
  id,
  status = true,
  register
}) => {

  const initialType = type
  const [isOpen, setIsOpen] = useState(type === 'password')
  const [typeInput, setType] = useState(type)

  const toggleOpenPassword = useCallback(() => {
    const typeUpdated = typeInput === 'password' ? 'text' : 'password'
    setType(typeUpdated)
    setIsOpen((value) => !value)
}, [typeInput])

  return (
    <div>
      <label className="flex items-center gap-1" htmlFor="">
      <input
        {...register(id)}
        id={id}
        type={typeInput}
        placeholder={label}
        className={`${
          !status.message
            ? `border-green-500 border`
            : `border-red-700 border-2`
        } px-4 py-2 rounded-lg w-full focus:outline-none`}
      />
      {initialType === "password" && isOpen && <FaRegEyeSlash className="text-3xl text-green-800 cursor-pointer" onClick={toggleOpenPassword}/>}
      {initialType === "password" && !isOpen && <FaRegEye className="text-3xl text-green-800 cursor-pointer" onClick={toggleOpenPassword}/>}
      </label>
    </div>
  );
};

export default Input;
