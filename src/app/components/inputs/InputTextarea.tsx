

const InputTextArea: React.FC<any> = ({label, type, id, register}) => {
    return (
      <div>
         <label htmlFor=""></label>
         <textarea {...register(id)} id={id} type={type} placeholder={label} className="text-black bg-lime-100 border-color border-red-300" />
      </div>
    )
  }
  
  export default InputTextArea