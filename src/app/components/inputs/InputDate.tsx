'use client'

const InputDate: React.FC<any> = ({label, id, status = true, register}) => {

    const handleDateChange = (date:any) => {
        const { value } = date.target;
      
        const formattedValue = value
          .replace(/\D/g, '')
          .replace(/(\d{2})(\d)/, '$1/$2')
          .replace(/(\d{2})(\d)/, '$1/$2')
          .replace(/(\d{4})(\d)/, '$1');
          date.target.value = formattedValue;
      };

    return (
      <div>
         <input {...register(id)} id={id} type="text" placeholder={label} onChange={handleDateChange} className={`${!status.message ? `border-green-500 border` : `border-red-700 border-2` } px-4 py-2 rounded-lg w-full focus:outline-none`} />
      </div>
    )
  }
  
  export default InputDate