'use client';


const NotificationAlert: React.FC<any> = ({numberAlert}) => {
    return (
        <div className="
        bg-red-500 
        text-white 
        px-2.5 
        font-bold 
        py-1 
        rounded-full 
        text-xs">
            {numberAlert}
        </div> 
      
    )
}

export default NotificationAlert