'use client'

import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import Input from "./inputs/Input";
import axios from "axios";
import Modal from "./Modal";
import useProfileModal from "../hooks/setProfileModal";
import { useRouter } from "next/navigation"
import { useState } from "react";
import Avatar from "./Avatar";
import Button from "./Button";

const Sidebar: React.FC<any> = ({userProfile}) => {
  

  
  // const onSubmit: SubmitHandler<FieldValues> = (data) => {

  //   const dataUpdate = {
  //     id:currentUser.id,
  //     name:data.name,
  //     lastName:data?.lastName,
  //     age: data.age,
  //     description: data.description
  //   }

  //   axios.put('/api/updateprofile', dataUpdate)
  //     .then((profile) => {
  //       modal.onClose()
  //       route.refresh()
  //       route.push('/')
       
  //     })
  //     .catch((error) => {
 
  //     })
  //     .finally(() => {
      
  //     })
  
  // }

  // const handleRefuse = (invite:any) => {
  //   axios.delete(`/api/invite/${invite.id}`)
  // }

  // const handleAcepted = (invite:any,indice:any) => {
     
  //   const dataUpdate = {
  //     userId:currentUser?.id,
  //     userName:currentUser?.name,
  //     groupName: invite.groupName,
  //     groupId:invite.groupId,
  //     priceMaximum: invite.priceMaximum,
  //     initialData: invite.initialData,
  //     description: invite.description,
  //     typeUser: "Normal"
  //   }

  //   axios.post('/api/accept', dataUpdate)
  //     .then((profile) => {
  //       currentUser.invites.splice(1,indice)
       
  //     })
  //     .catch((error) => {
 
  //     })
  //     .finally(() => {
      
  //     })


  //     axios.delete(`/api/invite/${invite.id}`)
    
  // }

    // const bodyContent = (
    //   <div>
    //    <Input
    //    label="Alterar Nome"
    //    type="text"
    //    id="name"
    //    register={register}
    //    />

    //    <Input
    //    label="Alterar Sobrenome"
    //    type="text"
    //    id="lastName"
    //    register={register}
    //    />

    //    <Input
    //    label="Alterar Idade"
    //    type="text"
    //    id="age"
    //    register={register}
    //    />

    //   <textarea {...register('description')} id="description"  placeholder="Nova Descrição" cols={10} rows={5}></textarea>
    //   </div>
      
    // )

    const onSubmit = (data:any) => {
 
     return false

    }


  return (
    <>
      <aside className="bg-white max-w-xs shadow-md min-h-dvh h-full pt-4">
        <section className="flex flex-direction flex-col gap-4 mb-4">
          <div className="mx-auto">
          <Avatar src={userProfile?.image} height={150} width={150}/>
          </div>
          <p className="mx-auto text-xl font-bold text-green-700">
            {userProfile?.name}{" "}
            {userProfile?.lastName}
          </p>
          <div className="mx-10">
          <Button name="Enviar Mensagem" onSubmit={()=>onSubmit}/>
          </div>
        </section>
        <hr />
        {/* <nav className="flex flex-direction flex-col">
          <ol>
            <li>Mensagens</li>
            <li>Notificações</li>
            {currentUser.invites.length > 0 && 
            currentUser.invites.map((item:any, indice:any)=>{
                return (
                  <section key={item.id}>
                      <div>{item.groupName}</div>
                      <div>{item.participants}</div>
                      <div>{item.priceMaximum}</div>
                      <div onClick={() => handleAcepted(item, indice)}>Aceitar</div>
                      <div onClick={() => handleRefuse(item)}>Recusar</div>
                  </section>
                
                )
            })
              
            }
            <li>Meus Grupos</li>
            <li>Lista de Desejos</li>
          </ol>
        </nav> */}
        
      </aside>
    </>
  );
};

export default Sidebar;
