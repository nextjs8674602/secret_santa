'use client'

import { AiOutlineMenu } from "react-icons/ai"
import Avatar from "./Avatar";
import MenuItem from "./MenuItem";
import { useRouter } from "next/navigation";
import { useCallback, useState } from "react";
import { signOut } from "next-auth/react";
import { CiMail } from "react-icons/ci";
import { IoIosNotificationsOutline } from "react-icons/io";
import { PiSignOutThin } from "react-icons/pi";
import { CiEdit } from "react-icons/ci";
import { CiUser } from "react-icons/ci";
import useProfileModal from "../hooks/setProfileModal";
import NotificationAlert from "./NotificationAlert";
import axios from "axios";





const UserMenu: React.FC<any> = ({currentUser}) => {
    console.log(currentUser)
    const [isOpen, setIsOpen] = useState(false)
    const modal = useProfileModal()
    const router = useRouter()

    const handleSignOut = () => {
        router.push('/')
          signOut()
      }

    const toggleOpen = useCallback(() => {
        setIsOpen((value) => !value)
    }, [])

    const handleNotifications = () => {
      const dataUpdated = {
        userId: currentUser.id,
        notifications:null
      }
      axios.put("/api/notification", dataUpdated)
      .then(() => {
        currentUser.notifications = null
      
      })
      .catch((error) => {})
      .finally(() => {});
      router.push('/notifications')
    }
  
  return (
    <div
    onClick={toggleOpen}
    className="
    p-4
    md:py-1
    md:px-2
    border-[1px]
    border-green-200
    flex
    flex-row
    items-center
    gap-3
    rounded-full
    cursor-pointer
    hover:shadow-md
    transition
    "
    >
    <AiOutlineMenu className="text-green-300"/>
    <div className="hidden md:block">
        <Avatar src={currentUser?.image} height={30} width={30}/>
    </div>
    {isOpen && (
                <div
                className="
                absolute
                rounded-xl
                shadow-md
                w-[40vw]
                md:w-2/12
                bg-white
                overflow-hidden
                right-0
                top-12
                text-sm
                "
                >
                 <div className="flex flex-col cursor-pointer">

                     <>
                     <MenuItem 
                       onClick={() => router.push(`/perfil/${currentUser.id}`)}
                       icon={<CiUser/>}
                       label="Minha Conta"
                     />
                     <MenuItem 
                       onClick={() => router.push('/edit')}
                       icon={<CiEdit/>}
                       label="Editar Conta"
                     />
                   
                     <MenuItem 
                       onClick={handleNotifications}
                       icon={<IoIosNotificationsOutline />}
                       numberAlert={currentUser.notifications}
                       label="Notificações"
                     />
                     
                     <MenuItem 
                       onClick={() => router.push('/favorites')}
                       icon={<CiMail/>}
                       label="Mensagens"
                     />
                     <hr />
                     <MenuItem 
                       onClick={handleSignOut}
                       icon={<PiSignOutThin/>}
                       label="Sair"
                     />
                    </>
                    
                    
                 </div>
                </div>
            )}
           
    </div>
  );
};

export default UserMenu;
