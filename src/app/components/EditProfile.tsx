'use client'

import { zodResolver } from "@hookform/resolvers/zod";
import axios from "axios";
import { usePathname, useRouter } from "next/navigation";
import { useState } from "react";

import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import { editUserFormSchema } from "../validations/formsValidations/editUserValidate";
import InputDate from "./inputs/InputDate";
import SubtitleValidation from "./SubtitleValidation";
import Input from "./inputs/Input";
import Button from "./Button";
import SpinnerLoad from "./SpinnerLoad";

const EditProfile: React.FC<any> = ({currentUser}) => {
    const [isLoading, setIsLoading] = useState(false)
    const route = useRouter()


    const defaultValues = {
      name: currentUser.name,
      lastName: currentUser.lastName,
      dateBirth: currentUser.dateBirth,
      description: currentUser.description
    };

    const {
      register,
      handleSubmit,
      formState: {
          errors,
      }
  } = useForm<FieldValues>({
    resolver: zodResolver(editUserFormSchema),
    defaultValues
  })
  
    const onSubmit: SubmitHandler<FieldValues> = (data) => {
 
      setIsLoading(true)
      const dataUpdate = {
        id:currentUser.id,
        name:data.name,
        lastName:data?.lastName,
        dateBirth: data.dateBirth,
        description: data.description
      }
      console.log('dataUpdate', dataUpdate)
  
      axios.put('/api/updateprofile', dataUpdate)
        .then((profile) => {
          route.push('/')
          setIsLoading(false)
        
        })
        .catch((error) => {
        })
        .finally(() => {
          setIsLoading(false)
        })

    }
  
    return (
        <div className="p-4 bg-white shadow-md rounded-md ">
            <h1 className="text-4xl font-bold text-green-700 mb-10">Editar Conta:</h1>
          <div className="grid grid-cols-2 gap-4">
            <div className="flex flex-col">
              <Input
                label="Alterar Nome"
                type="text"
                id="name"
                register={register}
              />
              {errors.name && <SubtitleValidation subtitle={errors.name.message} />}
            </div>
            <div className="flex flex-col">
              <Input
                label="Alterar Sobrenome"
                type="text"
                id="lastName"
                register={register}
              />
              {errors.lastName && <SubtitleValidation subtitle={errors.lastName.message} />}
            </div>
            <div className="flex flex-col">
              <InputDate
                label="Data de Nascimento"
                type="text"
                id="dateBirth"
                register={register}
              />
              {errors.dateBirth && <SubtitleValidation subtitle={errors.dateBirth.message} />}
            </div>
            <div className="flex flex-col col-span-2">
              <textarea
                className="border-green-500 border px-2 py-1 rounded-lg w-full focus:outline-none"
                {...register('description')}
                id="description"
                placeholder="Nova Descrição"
                cols={10}
                rows={5}
              ></textarea>
              {errors.description && <SubtitleValidation subtitle={errors.description.message} />}
            </div>
          </div>
          <div className="flex justify-end mt-4">
            {isLoading ? <SpinnerLoad /> 
             :
           <Button
            name="Salvar"
            onSubmit={handleSubmit(onSubmit)}
          />}
          
            
          </div>
        </div>
      );
};

export default EditProfile;
