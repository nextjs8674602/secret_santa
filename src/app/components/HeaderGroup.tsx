'use client'

import { FieldValues, SubmitHandler, useForm } from "react-hook-form"
import useGroupConfigModal from "../hooks/setConfigGroupModal"
import Modal from "./Modal"
import Input from "./inputs/Input"
import axios from "axios"
import { getPermissionGroup } from "../actions/getPermission"

const HeaderGroup: React.FC<any> = ({dataHeader, setGroup, currentUser}) => {
  

  const permission = getPermissionGroup(dataHeader.participants, currentUser?.id)
 

  const modal = useGroupConfigModal()

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      name: "",
      maximumPrice: "",
      initialData: "",
      description: ""
    },
  });

  const onSubmit: SubmitHandler<FieldValues> = (dataForm) => {

    const dataUpdated = {
      groupId: dataHeader.id,
      name: dataForm.name,
      maximumPrice: dataForm.maximumPrice,
      description: dataForm.description,
      initialData: dataForm.initialData
    }

    axios.put("/api/updategroup", dataUpdated)
      .then(({data}) => {
        
        setGroup({
           ...dataHeader,
           description: data.description,
           initialData: data.initialData,
           name:data.name,
           priceMaximum:data.priceMaximum
        })
        modal.onClose();
      })
      .catch((error) => {})
      .finally(() => {});

  };

  const bodyContent = (
    <div>
      <Input 
        label="Novo Nome" 
        type="text" 
        id="name" 
        register={register} 
     />

      <Input
        label="Nova Descrição"
        type="text"
        id="description"
        register={register}
      />

      <Input
        label="Novo Preço Máximo"
        type="text"
        id="maximumPrice"
        register={register}
      />

      <Input 
        label="Nova Data de Início" 
        type="text" 
        id="initialData" 
        register={register} 
      />
    </div>
  )

  const handleDraw = () => {
     
    const dataUpdate = {
      groupId: dataHeader.id,
      groupName: dataHeader.name
    }

  
    axios.post("/api/drawgroup", dataUpdate)
    .then(({data}) => {
      
   
   
    })
    .catch((error) => {})
    .finally(() => {console.log('finalizou')});
  }
    
      return (
       <section className="mt-10">
         <h1>{dataHeader.name}</h1>
         <div className="flex">
         <span>{dataHeader.participants.length}</span>
         <span>{dataHeader.priceMaximum}</span>
         <span>{dataHeader.initialData}</span>
         {permission && <div onClick={handleDraw}>Sortear</div>}
         {permission && <span onClick={modal.onOpen}>Configuração</span>}
         </div>
        <Modal
          isOpen={modal.isOpen}
          title="Configuração"
          body={bodyContent}
          onClose={modal.onClose}
          onSubmit={handleSubmit(onSubmit)}
        />
       </section>
      )
    }

export default HeaderGroup