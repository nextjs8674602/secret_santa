'use client'

import { FieldValues, SubmitHandler, useForm } from "react-hook-form"
import Input from "./inputs/Input"
import Button from "./Button"
import axios from "axios"

const SearchParticipant: React.FC<any> = ({group}) => {

    const {
        register,
        handleSubmit,
        formState: {
            errors,
        }
    } = useForm<FieldValues>({
      defaultValues: {
        email: ''
      }
    })

    const onSubmit: SubmitHandler<FieldValues> = async (data) => {
        const dataUpdated = {
          email: data.email,
          groupId: group.id,
          participants: group.participants.length,
          priceMaximum: group.priceMaximum,
          groupName: group.name,
          initialData: group.initialData,
          description: group.description,
          increase:1
        }
   

        axios.post('/api/invite', dataUpdated)
        .then((userCurrent) => {
          //  router.refresh()
    
        })
        .catch((error) => {
   
        })
        .finally(() => {
        
        })

        
    }

      return (
       <div>
         <Input id="email" register={register} label="Digite um email" type="text"/>
         <Button onSubmit={handleSubmit(onSubmit)} name="Convidar" />
         </div>
         

      )
    }

export default SearchParticipant