'use client'

interface DescriptionProps  {
  description: string | null | undefined
}


const Description: React.FC<any> = ( {description}) => {


      return (
        <section className="flex flex-col bg-white shadow-md p-7">
          <div className="flex">
          <h1 className="text-xl font-bold text-green-700">Descrição:</h1>
          </div>
            <div>{description}</div>
        </section>
       
    
      )
    }

    export default Description