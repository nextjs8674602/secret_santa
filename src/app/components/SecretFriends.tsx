"use client";

import SecretFriendList from "./SecretFriendList";


const SecretFriends: React.FC<any> = ({userProfile}) => {


  return (
   <section>
     <h1>Meus amigos secretos:</h1>
     <div className="flex gap-5">
          {userProfile.mySecretFriends.length > 0 ? (
             userProfile.mySecretFriends.map((secretFriend:any) => {
              return (
                 <SecretFriendList key={secretFriend.id} secretFriend={secretFriend}/>
              )  
            })
          ) : (
            <div>Nenhuma participação em grupos ainda.</div>
          )}
          </div>
   </section>
  )
};

export default SecretFriends;
