'use client'

import axios from "axios";
import Avatar from "./Avatar";
import { useState } from "react";
import Button from "./Button";

const UseNotifications: React.FC<any> = ({ currentUser }) => {
  const [invites, setInvites] = useState(currentUser.invites);
  const [removing, setRemoving] = useState<string | null>(null);
  const [nextCardIndex, setNextCardIndex] = useState<number | null>(null);

  const handleRefuse = (invite: any, index: number) => {
    setRemoving(invite.id);
    setNextCardIndex(index + 1);

    setTimeout(() => {
      axios.delete(`/api/invite/${invite.id}`)
        .then(() => {
          const invitesUpdated = invites.filter((item: any) => item.id !== invite.id);
          setInvites(invitesUpdated);
        })
        .catch((error) => {
          console.error(error);
        })
        .finally(() => {
          setRemoving(null);
          setNextCardIndex(null);
        });
    }, 500);
  };

  const handleAcepted = (invite: any, index: number) => {
    const dataUpdate = {
      userId: currentUser?.id,
      userName: currentUser?.name,
      groupName: invite.groupName,
      groupId: invite.groupId,
      priceMaximum: invite.priceMaximum,
      initialData: invite.initialData,
      description: invite.description,
      typeUser: "Normal"
    };

    axios.post('/api/accept', dataUpdate)
      .then(() => {
        const invitesUpdated = invites.filter((item: any) => item.id !== invite.id);
        setInvites(invitesUpdated);
      })
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        axios.delete(`/api/invite/${invite.id}`);
      });
  };

  return (
    <div className="w-full md:p-4 max-w-md mx-auto">
      {invites.map((item: any, index: number) => (
        <section
          key={item.id}
          className={`bg-white shadow-md rounded-lg  p-4 mb-4 flex items-center justify-between gap-6 transition-all duration-500 ease-in-out ${removing === item.id ? 'opacity-0 scale-95' : 'opacity-100 scale-100'} ${nextCardIndex === index ? 'delay-300' : 'delay-0'}`}
          style={{ transitionDelay: ``  }}
        >
          <div className="flex-1">
            <div className="text-green-700 font-semibold mb-2">Novo Grupo</div>
            <div className="flex items-center gap-4">
         
              <Avatar height={70} width={70} src={currentUser?.image} />
              <div className="flex items-baseline w-3/4">
                <div>
                  <div className="font-bold text-lg">{item.groupName}</div>
                  <div className="text-gray-600">Participants: {item.participants}</div>
                  <div className="text-gray-600">Maximum Price: {item.priceMaximum}</div>
                </div>
              </div>
     
              <div className="flex w-1/4 flex-col gap-3 justify-center ml-5">
                <Button name="Aceitar" onSubmit={() => handleAcepted(item, index)}/>
                <Button type="error" name="Recusar" onSubmit={() => handleRefuse(item, index)}/>
              </div>
            </div>
          </div>
        </section>
      ))}
      {invites.length === 0 && <div>Este usuário não possui notificações.</div>}
    </div>
  );
};

export default UseNotifications;