'use client';

import NotificationAlert from "./NotificationAlert";

interface MenuItemProps {
    onClick: () => void;
    label:string,
    icon?:any,
    numberAlert?:number
}

const MenuItem: React.FC<MenuItemProps> = ({
    onClick,
    label,
    icon,
    numberAlert
}) => {
    return (
        <div
        onClick={onClick}
        className="flex gap-4 items-center px-4
        py-3
        hover:bg-green-100
        transition
        font-semibold">
      <div className="text-xl">
       {icon}
      </div>
     <div
      >
        {label}
      </div>
    {numberAlert !== null && numberAlert !== undefined && <NotificationAlert numberAlert={numberAlert}/>}
        </div>
      
    )
}

export default MenuItem