'use client'

import SpinnerLoad from "./SpinnerLoad";


const Loader: React.FC<any> = () => {
  

    return (
        <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center bg-white z-50">
          <div className="bg-white p-8 rounded-lg shadow-lg">
            <div className="flex justify-center gap-5 items-center mb-4">
            <SpinnerLoad />
              <h2 className="text-xl font-semibold">Aguarde...</h2>
            </div>
            <p className="text-gray-600">Carregando seus dados...</p>
          </div>
        </div>
      );

}
  
  export default Loader;