'use client'


  const SubtitleValidation: React.FC<any> = ({subtitle}) => {   
    
      return (
           <span className="text-red-600 m-0">{subtitle}</span>
      )
    }
    
export default SubtitleValidation