'use client'

import { useCallback } from "react";

interface ButtonProps {
    name: string,
    onSubmit: () => void,
    size?: string,
    type?:string
  }


  const Button: React.FC<ButtonProps> = ({name,onSubmit,size = 'w-full', type = "success"}) => {
    let typeUI = ""
    
    if(type === "error"){
      typeUI = 'bg-red-500 hover:bg-red-600'
    }
    else if(type === "success"){
      typeUI = 'bg-green-500 hover:bg-green-600'
    }

    const handleSubmit = useCallback(() => {
      onSubmit();
    }, [onSubmit]);
    
      return (
        <div>
          <button onClick={handleSubmit} className={`${size} ${typeUI} relative text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline transition-colors duration-300`}>{name}</button>
        </div>
      )
    }
    
    export default Button