'use client'

import { useRouter } from "next/navigation"
import useModal from "../hooks/setGroupModal"
import { Groups } from "../types/group"
import Modal from "./Modal"
import Avatar from "./Avatar"

    const GroupList: React.FC<Groups> = ({group}) => {
    const route = useRouter()

    return (
      <section 
          onClick={() => route.push(`/group/${group.groupId}`)} 
          className="
              cursor-pointer
              p-4
              border
              rounded-lg
              shadow-md
              hover:shadow-lg
              transition-shadow
              bg-white
              mb-4
              flex
              flex-col
              gap-4
              items-center
              text-center
              "
      >
          <Avatar src={group.name} height={120} width={130}/>
    
          <h1 className="text-xl font-bold">{group.name}</h1>
          <p className="text-gray-600">{group.description}</p>
          <p className="text-gray-800 font-semibold">Usuário: {group.typeUser}</p>
      </section>
  );
    }

    export default GroupList