"use client";

import { SafeUser } from "../types/user";
import Logo from "./Logo";
import UserMenu from "./UserMenu";
import Container from "./Container";

const Navbar: React.FC<SafeUser> = ({ currentUser }) => {
  
  return (
    <header className="fixed bg-white shadow-md w-full border-b border-green-500 py-4 z-25">
      <Container>
        <div className="flex justify-between">
          <Logo />
          <UserMenu currentUser={currentUser} />
        </div>

        {/* {user && <div>{user.name}, <span onClick={handleSignOut}>Sair</span></div>} */}
      </Container>
    </header>
  );
};

export default Navbar;
