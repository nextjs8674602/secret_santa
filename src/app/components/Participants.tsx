'use client'

import { getPermissionGroup } from "../actions/getPermission"
import ParticipantsList from "./ParticipantsList"
import SearchParticipant from "./SearchParticipants"

const Participants: React.FC<any> = ({group, currentUser}) => {

  const permission = getPermissionGroup(group.participants, currentUser.id)


      return (
       <div>
         <h1>Participantes:</h1>
         {permission &&  <SearchParticipant group={group}/>}
         {group.participants.map((participant:any)=>{
            return(
                <ParticipantsList participant={participant} key={participant.id}/> 
            )
         })}

         </div>
         

      )
    }

export default Participants