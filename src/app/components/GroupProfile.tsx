'use client'

import Description from "@/app/components/Description";
import Sidebar from "@/app/components/Sidebar";
import axios from "axios";
import { useEffect, useState } from "react";
import { usePathname, useRouter, useSearchParams } from "next/navigation"
import Participants from "@/app/components/Participants";
import HeaderGroup from "@/app/components/HeaderGroup";
import NotFound from "../not-found"


const GroupProfile: React.FC<any> = ({currentUser}) => {
 
  const groupId = usePathname()?.split('/')[2]
  const [group, setGroup] = useState<any>({
      id: "",
      description: "",
      initialData: "",
      name: "",
      participants: [],
      priceMaximum:""
  })
  
  
  useEffect(()=>{
    axios.post('/api/group', groupId)
    .then((groupData:any) => {
      setGroup(groupData.data)
    })
    .catch((error) => {

    })
    .finally(() => {
      
    })
  },[groupId])

  
  return (
    <div className="flex ">
      {group.id ? 
       <>
        <div className="flex flex-col">
        <HeaderGroup  dataHeader={group} setGroup={setGroup} currentUser={currentUser}/>
        <div className="flex gap-10 flex-col">
        <Description description={group.description}/>
        <Participants group={group} currentUser={currentUser}/>
        <hr />
      </div>
        </div>
       </>
       : 
       <NotFound />
    }
        
        
    </div>
  );
};

export default GroupProfile;
