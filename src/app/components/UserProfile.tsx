'use client'

import Description from "@/app/components/Description";
import Gift from "@/app/components/Gift";
import Groups from "@/app/components/Groups";
import Sidebar from "@/app/components/Sidebar";
import axios from "axios";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import HeaderUserProfile from "./HeaderUserProfile";
import SecretFriends from "./SecretFriends";
import NotFound from "../not-found";
import useLoading from "../hooks/setLoading";
import Loader from "./Loader";

const Perfil: React.FC<any> = ({currentUser}) => {
    
    const userId:any = usePathname()?.split('/')[2]
    const [loading, setLoading] = useState<any>(true)
    const [pageNotFound, setPageNotFound] = useState<any>(false)
    const [userProfile, setUserProfile] = useState<any>({
        id:null,
        name: "",
        lastName: "",
        age: "",
        email: "",
        description: "",
        myGroups: [],
        myGifts: [],
        mySecretFriends: [],
        invites: [],
        permission:false
    })
   
    

    useEffect(()=>{
      
        const dataUpdated = {
            userLoggedId:currentUser.id,
            userProfileId:parseInt(userId)
        }
        axios.post('/api/userprofile', dataUpdated)
        .then((userData:any) => {
          setLoading(false)
          if(userData.data === null){
            setPageNotFound(true)
            return
          }
          setUserProfile(userData.data)
           
        })
        .catch((error) => {
          
        })
        .finally(() => {
          
        })
      },[userId, currentUser.id])

  
  return (
    <div>
      {userProfile.id && 
      <>
      <div className="flex gap-40">
        <div>
      <Sidebar userProfile={userProfile} />
        </div>
   
        <div className="flex gap-10 flex-col mx-auto">
        <HeaderUserProfile userProfile={userProfile}/>
        <Description description={userProfile.description} />
        <hr />
        <Groups userProfile={userProfile} />
        <hr />
        <Gift userProfile={userProfile}  />
        <hr />
        <SecretFriends userProfile={userProfile}/> 

        </div>
      
      </div>
       
      </>
  }
      {pageNotFound && <NotFound />}
      
      {loading && <Loader />}
      
    </div>
  );
};

export default Perfil;
