"use client";

import { Gift } from "../types/gift";

const GiftList: React.FC<Gift> = ({ gift }) => {

  return (
    <section className="flex">
      <h1>{gift.name}</h1>
      <p>{gift.avaragePrice}</p>
      <p>Link: {gift.link}</p>
    </section>
  );
};

export default GiftList;