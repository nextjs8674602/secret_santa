"use client";

import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import Input from "./inputs/Input";
import Modal from "./Modal";
import useGiftModal from "../hooks/setGiftModal";
import axios from "axios";
import GiftList from "./GiftList";
import { User } from "../types/user";
import { GiftProperty } from "../types/gift";

interface GiftProps {
  gifts:GiftProperty[],
  user: User
}

const Gift: React.FC<any> = ({ userProfile }) => {
  const modal = useGiftModal();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      name: "",
      avaragePrice: "",
      link: "",
    },
  });

  const onSubmit: SubmitHandler<FieldValues> = (data) => {

    const dataUpdate = {
        userId:userProfile?.id,
        name: data.name,
        avaragePrice: data.avaragePrice,
        link: data.link
      }

    axios
      .post("/api/gift", dataUpdate)
      .then((gifts) => {

        modal.onClose();
      })
      .catch((error) => {})
      .finally(() => {});
    modal.onClose();
  };

  const bodyContent = (
    <div>
      <Input 
        label="Nome" 
        type="text" 
        id="name" 
        register={register} 
      />

      <Input
        label="Preço Médio"
        type="text"
        id="avaragePrice"
        register={register}
      />

      <Input 
        label="Link" 
        type="text" 
        id="link" 
        register={register} 
      />
    </div>
  );

  return (
    <section className="flex">
      <h1>Lista de Desejos:</h1>
      <div onClick={modal.onOpen}>[Adicionar]</div>
      <div className="flex gap-5">
          {userProfile.myGifts.length > 0 ? (
             userProfile.myGifts.map((gift:any) => {
              return (
                 <GiftList key={gift.id} gift={gift}/>
              )  
            })
          ) : (
            <div>Nenhuma participação em grupos ainda.</div>
          )}
          </div>
      <Modal
        isOpen={modal.isOpen}
        title="Presente"
        body={bodyContent}
        onClose={modal.onClose}
        onSubmit={handleSubmit(onSubmit)}
      />
    </section>
  );
};

export default Gift;
