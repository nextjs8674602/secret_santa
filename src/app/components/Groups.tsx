'use client'

import { FieldValues, SubmitHandler, useForm } from "react-hook-form"
import useGroupModal from "../hooks/setGroupModal"
import Modal from "./Modal"
import Input from "./inputs/Input"
import axios from "axios"
import GroupList from "./GroupList"
import { GroupProperty } from "../types/group"
import { User } from "../types/user"
import Button from "./Button"


interface GroupProps {
  groups:GroupProperty[],
  user: User
}

const Groups:React.FC<any> = ({userProfile}) => {
  
    const modal = useGroupModal()


    const {
      register,
      handleSubmit,
      formState: {
          errors,
      }
  } = useForm<FieldValues>({
    defaultValues: {
      name: '',
      priceMaximum: '',
      initialData: '',
    }
  })

  const onSubmit: SubmitHandler<FieldValues> = (data) => {
    const dataUpdate = {
      userId:userProfile?.id,
      userName:userProfile?.name,
      groupName: data.name,
      priceMaximum: data.priceMaximum,
      initialData: data.initialData,
      description: data.description,
      setGroup: true,
      typeUser: "Administrador"
    }

    axios.post('/api/mygroups', dataUpdate)
      .then((myGroups) => {
        userProfile.myGroups = myGroups.data
       
        modal.onClose()

      })
      .catch((error) => {
 
      })
      .finally(() => {
      
      })
  
  }

    const bodyContent = (
      <div>
       <Input
       label="Nome"
       type="text"
       id="name"
       register={register}
       />

       <Input
       label="Preço Máximo"
       type="text"
       id="priceMaximum"
       register={register}
       />

       <Input
       label="Data Inicial"
       type="text"
       id="initialData"
       register={register}
       />

      <textarea {...register('description')} id="description"  placeholder="Descrição do Grupo" cols={10} rows={5}></textarea>
      </div>
      
    )
    
      return (
        <section className="bg-white shadow-md p-7">
          <div className="flex mb-10 justify-between items-center cursor-pointer">
          <h1 className="text-xl font-bold text-green-700">Meus Grupos:</h1>
          {userProfile.permission && <Button name="Criar Grupo" size="w-full" onSubmit={modal.onOpen}/>}
          </div>
          <div className="flex gap-5">
          {userProfile.myGroups.length > 0 ? (
             userProfile.myGroups.map((group:any) => {
              return (
                 <GroupList key={group.id} group={group}/>
              )  
            })
          ) : (
            <div>Nenhuma participação em grupos ainda.</div>
          )}
          </div>
          
          <Modal 
            isOpen={modal.isOpen}
            title="Grupos"
            body={bodyContent}
            onClose={modal.onClose}
            onSubmit={handleSubmit(onSubmit)}
          />
        </section>
      )
    }

export default Groups