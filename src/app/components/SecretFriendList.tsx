"use client";


const SecretFriendsList: React.FC<any> = ({secretFriend}) => {


  return (
   <section>
    <h1>{secretFriend.secretFriendName}</h1>
    <p>{secretFriend.groupName}</p>
   </section>
  )
};

export default SecretFriendsList;
