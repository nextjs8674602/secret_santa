import getCurrentUser from "@/app/actions/getCurrentUser";
import Navbar from "@/app/components/Navbar";
import Sidebar from "@/app/components/Sidebar";
import UserProfile from "@/app/components/UserProfile";

const Perfil: React.FC = async () => {
  const currentUser = await getCurrentUser();
  console.log('currentUser',currentUser)
  
  return (
    <div className="min-h-full">
      <Navbar currentUser={currentUser}/>
      <div className="pt-20 flex justify-between w-full min-h-full">
      <UserProfile currentUser={currentUser}/>
      </div>
    </div>
  );
};

export default Perfil;
