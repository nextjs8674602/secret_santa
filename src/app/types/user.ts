
export type User = {
  name: string | null,
  lastName: string | null,
  age: string | null,
  id:number | null,
  description:string | null,
  notifications:string[] | null,
  password?:string | null,
  email:string | null
} | null

export interface SafeUser {
  currentUser: User | null
}