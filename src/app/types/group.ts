export type GroupProperty = {
  name:string,
  initialData:string,
  description:string,
  priceMaximum:number,
  typeUser:string,
  id:number,
  groupId:number
  } 

export type Groups = {
  group: GroupProperty
}  