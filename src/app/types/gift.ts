export type GiftProperty= {
    name:string,
    link:string,
    id:number,
    userId:number
    avaragePrice:number,
} 

export type Gift = {
    gift:GiftProperty,
  
} 