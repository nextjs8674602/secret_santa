'use client'

import { useRouter } from "next/navigation"

import { FieldValues, SubmitHandler, useForm } from "react-hook-form"
import axios from "axios"
import { signIn } from "next-auth/react"
import { SafeUser, User } from "../types/user"
import Image from 'next/image'
import { useState } from "react"
import { zodResolver } from "@hookform/resolvers/zod"
import { loginFormSchema } from "../validations/formsValidations/loginValidate"
import SubtitleValidation from "../components/SubtitleValidation"
import SpinnerLoad from "../components/SpinnerLoad"
import Button from "../components/Button"
import Input from "../components/inputs/Input"
import useLoading from "../hooks/setLoading"



const Login:React.FC<SafeUser> = () => {
  
  const router = useRouter()
  const [loading, setLoading]= useState(false)
  const [statusLogin, setStatusLogin]= useState({
    status: true,
    message: ""
  })
  const loadingPage = useLoading()

  const {
    register,
    handleSubmit,
    formState: {
        errors,
    }
} = useForm<FieldValues>({
  resolver: zodResolver(loginFormSchema)
})

    
  //   const {
  //     register,
  //     handleSubmit,
  //     formState: {
  //         errors,
  //     }
  // } = useForm<FieldValues>({
  //   defaultValues: {
  //     email: '',
  //     password: ''
  //   }
  // })


    const onSubmit: SubmitHandler<FieldValues> = async (data) => {
      setLoading(true)
      signIn('credentials', {
        ...data,
        redirect: false
      })
      .then((log) => {
        setLoading(false)
        if(!log?.ok){
          const message = "Email ou senha não válidos!"
          const statusLoginUpdated = {...statusLogin}
          statusLoginUpdated.status = false
          statusLoginUpdated.message = message
          setStatusLogin(statusLoginUpdated)
          return
        }
        loadingPage.onOpen()
        router.push('/')
        
      })
      .catch((err) => {
        console.error('error',err);
          const message = "Instabilidade no servidor, por favor tente mais tarde."
          const statusLoginUpdated = {...statusLogin}
          statusLoginUpdated.status = false
          statusLoginUpdated.message = message
          setStatusLogin(statusLoginUpdated)
          return
      });

      
  }
    
    return (
      <section className="flex h-screen bg-slate-50">
        <div className="items-center h-screen hidden md:flex w-2/3 justify-center">
        <Image src="/logo.png" alt="Descrição da Imagem" width={300} height={100} />
        </div>
      <div className="flex flex-col justify-center border-l-2 shadow-2xl border-green-700 p-10 w-dvw h-screen md:w-1/3">
        <h1 className="font-bold text-4xl text-center mb-10 flex-2 text-green-700">Login</h1>
        <div className="flex flex-col gap-4">
          <div>
          <Input status={errors.email} id="email" register={register} type="email" label="Email"/>
         {errors.email && <SubtitleValidation subtitle={errors.email.message}/>} 
          </div>
          <div>
          <Input status={errors.password} id="password" register={register} type="password" label="Password"/>
         {errors.password && <SubtitleValidation subtitle={errors.password.message}/>} 
          </div>

        {!statusLogin.status && <span className="bg-red-100 mt-5 border text-center border-red-400 text-red-700 px-4 py-2 rounded-md">{statusLogin.message}</span>} 
        {loading ? 
        <SpinnerLoad/> : 
        <Button
          onSubmit={handleSubmit(onSubmit)} 
          name="Entrar"
        />
        }
        

        <p className="mt-3 text-center">Não possui cadastro? <span onClick={()=>router.push('/register')} className="text-green-600 hover:text-green-500 cursor-pointer transition-colors duration-200">Cadastre-se</span></p>
        </div>
  
         
      </div>
      </section>
    )
  }
  
  export default Login