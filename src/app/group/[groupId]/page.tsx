
import getCurrentUser from "@/app/actions/getCurrentUser";
import { redirect } from "next/navigation"
import GroupProfile from "@/app/components/GroupProfile";
import Navbar from "@/app/components/Navbar";
import Sidebar from "@/app/components/Sidebar";


const GroupPage: React.FC = async () => {
  const currentUser = await getCurrentUser()
  
  // if(!currentUser){
  //   redirect("/");
  // }
  
  
  return (
    <div>
      <Navbar currentUser={currentUser}/>
      <div className="pt-20 flex">
      <Sidebar currentUser={currentUser} />
        <GroupProfile currentUser={currentUser}/>
      </div>
        
    </div>
  );
};

export default GroupPage;
