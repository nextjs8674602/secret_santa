import { z } from "zod";

export const loginFormSchema: any = z
  .object({
    email: z
      .string()
      .email("Por favor, insira um endereço de e-mail válido.")
      .toLowerCase(),
    password: z.string().min(6, "Insira no mínimo 6 caracteres.")
  })