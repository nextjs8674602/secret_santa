import { setFormattedDate } from "@/app/actions/getFormattedDate";
import { getValidateDate } from "@/app/actions/getValidateDate";
import { z } from "zod";

export const registerFormSchema = z
  .object({
    name: z
      .string()
      .min(2, "Mínimo dois caracteres.")
      .max(20, "Máximo vinte caracteres."),
    lastName: z
      .string()
      .min(2, "Mínimo dois caracteres.")
      .max(20, "Máximo vinte caracteres."),
    dateBirth: z
      .string().refine((date) => validateDateBirth(date), 
      { message: "Essa data está incorreta, por favor altere para uma data válida."}),
    email: z
      .string()
      .email("Por favor, insira um endereço de e-mail válido.")
      .toLowerCase(),
    password: z.string().min(6, "Insira no mínimo 6 caracteres."),
    confirmPassword: z.string().min(6, "Insira no mínimo 6 caracteres."),
  })
  .refine((data) => data.password === data.confirmPassword, {
    message: "As senhas não correspondem.",
    path: ["confirmPassword"],
  });


  export const validateDateBirth = (dateBirth:string) => {
    const formattedDate = setFormattedDate(dateBirth)
    const currentDate = new Date();
    const selectedDate = new Date(formattedDate)
    const selectedYear = Number(dateBirth.split('/')[2])


    return (
      selectedDate.getTime() < currentDate.getTime() &&
      selectedYear >= 1900 && getValidateDate(dateBirth) &&
      /^\d{2}\/\d{2}\/\d{4}$/.test(dateBirth)
    );
  }


  
