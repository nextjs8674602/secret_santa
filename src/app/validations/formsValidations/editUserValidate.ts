import { z } from "zod";
import { validateDateBirth } from "./registerValidate";

export const editUserFormSchema: any = z
  .object({
    name: z
      .string()
      .min(2, "Mínimo dois caracteres.")
      .max(20, "Máximo vinte caracteres."),
      lastName: z
      .string()
      .min(2, "Mínimo dois caracteres.")
      .max(20, "Máximo vinte caracteres."),
      dateBirth: z
      .string().refine((date) => validateDateBirth(date), 
      { message: "Essa data está incorreta, por favor altere para uma data válida."}),
      description: z
      .string()
      .min(5, "Mínimo 5 caracteres.")
      .max(3000, "Máximo 3000 caracteres.")
  })