export default function gettingEmailValidation(email:string) {
    const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/

    return regexEmail.test(email)
}