export const getPermissionGroup = (participants:any, userId:any) => {

    if(!participants && !userId){
       return false
    }


   const validacao = participants.some((participant:any) => {
        return participant.userId === userId && participant.userType === "Administrador"
    })

    return validacao
}