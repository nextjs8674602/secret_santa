import { getServerSession } from "next-auth/next"

import { authOptions } from "@/pages/api/auth/[...nextauth]";
import prisma from "@/app/libs/prismadb";
import { User } from "../types/user";
import { getFormattedDate } from "./getFormattedDate";

export async function getSession() {
  
  return await getServerSession(authOptions)
}

export default async function getCurrentUser() {
  try {
      const session = await getSession();

    if (!session?.user?.email) {
      return null;
    }
    const currentUser = await prisma.user.findUnique({
      where: {
        email: session.user.email as string,
      }
    });

    if (!currentUser) {
      return null;
    }

    const getInvites = await prisma.invite.findMany({
      where: {
        userId: currentUser.id
      }
    })

    
    
    const safeUser: any = currentUser
    const dateFormatted = getFormattedDate(currentUser.dateBirth)
    
    safeUser.invites = getInvites
    safeUser.dateBirth = dateFormatted
    delete safeUser.password

    return safeUser
    
  } catch (error: any) {
    return null;
  }
}