export const getValidateDate = (date:string) => {
    let validation = false
    const selectedDay = Number(date.split('/')[0])
    const selectedMonth = Number(date.split('/')[1])
    const selectedYear = Number(date.split('/')[2])

    if(selectedDay < 1 || selectedDay > 31) return true

    if(selectedMonth < 1 || selectedMonth > 12) return true

    const months31 = [1,3,5,7,8,10,12]

    if(selectedMonth === 2 && (selectedYear % 4 === 0)){
      validation = selectedDay > 29
    }
    else if(selectedMonth === 2){
      validation = selectedDay > 28
    }

    if(selectedDay === 31){
      validation = months31.some((month31) => selectedMonth === month31)
    }

    return !validation

  }