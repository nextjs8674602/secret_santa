export default async function getMyGifts(userId: number) {
  try {

    const gifts = await prisma?.gifts.findMany({
      where: {
        userId: userId,
      },
    });

    return gifts;
  } catch (error: any) {
    throw new Error(error);
  }
}
