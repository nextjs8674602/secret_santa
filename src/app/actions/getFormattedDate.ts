//"dd/mm/yyyy to yyyy-mm-dd"
export const setFormattedDate = (date:string) => {
    return date.split('/').reverse().join('-')
}

export const getFormattedDate = (date:any) => {
    const formattedDate = new Date(date);

    const day = String(formattedDate.getDate()).padStart(2, '0');
    const month = String(formattedDate.getMonth() + 1).padStart(2, '0'); // Os meses são indexados em 0
    const year = formattedDate.getFullYear();

    return `${day}/${month}/${year}` 
}