export default async function getMyGroups(userId:number) {

    try {
        const groups = await prisma?.myGroups.findMany({
            where: {
                userId: userId
            },
        })

        return groups
    } catch (error: any) {
        throw new Error(error)
    }
}