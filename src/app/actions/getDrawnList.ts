export const getDrawnList = (participants: any, groupId: any, groupName: any) => {
  let drawFriendGive = [...participants];
  let drawFriendReceive = [...participants];
  let drawnList = [];

  //To validate duplication in end of the drawn
  let userIdsGive:number[] = []
  let userIdsReceive:number[] = []

  while (drawFriendGive.length > 0) {
    
    let indexFriendGive = Math.floor(Math.random() * drawFriendGive.length);
    let indexFriendReceive = Math.floor(Math.random() * drawFriendReceive.length);

    if (drawFriendGive[indexFriendGive].userId !== drawFriendReceive[indexFriendReceive].userId) {
      if((drawFriendGive.length !== 2) || 
        (userIdsGive.indexOf(drawFriendGive[indexFriendGive].userId) === -1 && 
        userIdsReceive.indexOf(drawFriendGive[indexFriendGive].userId) === -1) ||
        (userIdsGive.indexOf(drawFriendReceive[indexFriendReceive].userId) === -1 && 
        userIdsReceive.indexOf(drawFriendReceive[indexFriendReceive].userId) === -1)){
          
      drawnList.push({
        groupId,
        groupName,
        userId: drawFriendGive[indexFriendGive].userId,
        secretFriendId: drawFriendReceive[indexFriendReceive].userId,
        secretFriendName: drawFriendReceive[indexFriendReceive].name,
      });

      userIdsGive.push(drawFriendGive[indexFriendGive].userId)
      userIdsReceive.push(drawFriendReceive[indexFriendReceive].userId)

      drawFriendGive.splice(indexFriendGive, 1);
      drawFriendReceive.splice(indexFriendReceive, 1);
    }
  }
}
  
  return drawnList;
};

