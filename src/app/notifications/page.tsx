import getCurrentUser from "@/app/actions/getCurrentUser";
import Navbar from "@/app/components/Navbar";
import UserProfile from "@/app/components/UserProfile";
import UserNotifications from "../components/UserNotifications";
import Sidebar from "../components/Sidebar";

const Notifications: React.FC = async () => {
  const currentUser = await getCurrentUser();
  
  
  return (
    <div>
      <Navbar currentUser={currentUser}/>
  

      <div className="pt-20 flex w-full">
      <UserNotifications currentUser={currentUser}/>
 
      </div>
    </div>
  );
};

export default Notifications;
