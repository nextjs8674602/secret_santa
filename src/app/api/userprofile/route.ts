import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";

export async function POST(
  request: Request, 
) {
  const body = await request.json();

  const { userProfileId, userLoggedId } = body

 
  const user = await prisma.user.findUnique({
    where: {
        id:userProfileId,
    },
  });

  const myGroups = await prisma.myGroups.findMany({
    where:{
        userId: userProfileId
    }
  })

  const myGifts = await prisma.gifts.findMany({
    where:{
        userId: userProfileId
    }
  })

  const mySecretFriends = await prisma.secretFriends.findMany({
    where: {
      userId: userProfileId
    }
  })

  const userProfile = user === null ? null : 
  {
    id:user?.id,
    name: user?.name,
    lastName: user?.lastName,
    dateBirth:user?.dateBirth,
    email: user?.email,
    description:user?.description,
    myGroups,
    myGifts,
    mySecretFriends,
    permission: userLoggedId === userProfileId
  }

  return NextResponse.json(userProfile);
}