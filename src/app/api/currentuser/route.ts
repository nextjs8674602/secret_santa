import { NextResponse } from "next/server";
import  bcrypt  from 'bcrypt'

import prisma from "@/app/libs/prismadb";
import getCurrentUser from "@/app/actions/getCurrentUser";


export async function POST(
    request: Request, 
    ) {
     
        const body = await request.json();

  const { 
    email
   } = body;

    const user = await prisma.user.findUnique({
        where: {
            email: email
        }
      });
    
  return NextResponse.json(user);
}