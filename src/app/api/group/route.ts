import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";

export async function POST(
  request: Request, 
) {
  const groupId = await request.json();
  
 
  const group = await prisma.group.findUnique({
    where: {
        id:groupId,
    },
  });

  const participants = await prisma.participants.findMany({
    where: {
      groupId,
    },
  });

  const groupsUpdated:any = group
  groupsUpdated.participants = participants

  return NextResponse.json(groupsUpdated);
}