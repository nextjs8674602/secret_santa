import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";

export async function POST(
    request: Request, 
  ) {
    const body = await request.json();

   const {groupId,userId, userName, groupName, priceMaximum, initialData, description,typeUser} = body
    
   
   await prisma.myGroups.create({
    data: {
        userId,
        name: groupName,
        groupId,
        priceMaximum: parseFloat(priceMaximum),
        initialData,
        description,
        typeUser
    },
  })

  await prisma.participants.create({
    data: {
        userId,
        name: userName,
        groupId,
        userType: typeUser  
    },
  })
  
   return NextResponse.json({});
  }