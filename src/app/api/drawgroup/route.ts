import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";
import { getDrawnList } from "@/app/actions/getDrawnList";

export async function POST(
  request: Request, 
) {

  const body = await request.json();

  const { groupId, groupName } = body

   const participants = await prisma.participants.findMany({
    where: {
        groupId
    }
  })

  const drawnList = getDrawnList(participants,groupId, groupName)
  
  await Promise.all(drawnList.map(async (objeto) => {
    await prisma.secretFriends.create({
      data: {
        groupId: objeto.groupId,
        groupName: objeto.groupName,
        userId: objeto.userId, 
        secretFriendId: objeto.secretFriendId,
        secretFriendName: objeto.secretFriendName
      },
    });
  }));
  
  return NextResponse.json(participants);

}