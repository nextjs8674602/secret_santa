import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";
import getMyGifts from "@/app/actions/getMyGifts";

export async function POST(
  request: Request, 
) {
  const body = await request.json();

  const { 
    userId,
    name,
    avaragePrice,
    link
   } = body;
 
   await prisma.gifts.create({
    data: {
        name,
        userId,
        avaragePrice:parseFloat(avaragePrice),
        link,
    }
  })

  const myGifts = await getMyGifts(userId)


  return NextResponse.json(myGifts);
}