import getCurrentUser from "@/app/actions/getCurrentUser"
import { NextResponse } from "next/server"
import prisma from "@/app/libs/prismadb"



interface IParams {
    inviteId?: any
}

export async function DELETE(
    request: Request, 
    { params }: { params: IParams}
  ) {

    const { inviteId } = params

    await prisma.invite.delete({
      where:{
        id:parseInt(inviteId)
      }
    })
  
    return NextResponse.json({});
  }