import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";

export async function POST(
    request: Request, 
  ) {
    const body = await request.json();

   const {email, groupId, participants, groupName, priceMaximum, initialData, description, increase} = body
    
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    
    if(!user){
      return
    }

    const participant = await prisma.participants.findFirst({
      where:{
        userId: user.id,
        groupId
      }
    })
    console.log('passou participant')
    if(participant){
      return
    }
    console.log('passou participant')
    const invite = await prisma.invite.findFirst({
      where:{
        userId: user.id,
        groupId
      }
    })

    if(invite){
      return
    }
    console.log('passou invite')
    
    
    await prisma.invite.create({
        data:{
          groupId,
          userId: user.id,
          participants,
          groupName,
          priceMaximum,
          initialData,
          description
        }
    })

    const notificationTotal = user.notifications === null ? 0 : user.notifications

    await prisma.user.update({
      where: {
        id:user.id
      },
      data:{
        notifications: notificationTotal + 1
      }
    })
  
  
    return NextResponse.json({});
  }


  