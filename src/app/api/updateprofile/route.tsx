import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";

export async function PUT(
  request: Request, 
) {
  const body = await request.json()

  const {name, lastName, age, description, id } = body
  
 
  const user = await prisma.user.update({
    where: {
        id
    },
    data: {
      description,
      name,
      lastName,
      age
    }
  });


  return NextResponse.json(user);
}