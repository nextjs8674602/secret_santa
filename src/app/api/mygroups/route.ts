import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";
import getMyGroups from "@/app/actions/getMyGroups";

export async function POST(
  request: Request, 
) {
  const body = await request.json();
  const { 
    userId,
    userName,
    groupName,
    priceMaximum,
    initialData,
    description,
    typeUser
   } = body; 

   
   let groupId
   

  await prisma.group.create({
    data: {
      name: groupName,        
      initialData,
      description,    
      priceMaximum: parseFloat(priceMaximum)
    }
  }).then(async (data)=> {
    groupId = data.id
    
  })

 if(!groupId) return

  await prisma.participants.create({
    data: {
      name: userName,        
      userId,
      groupId,    
      userType: typeUser  
    }
  })

  await prisma.myGroups.create({
  data: {
      userId,
      name: groupName,
      groupId,
      priceMaximum: parseFloat(priceMaximum),
      initialData,
      description,
      typeUser:typeUser
  },
})

const myGroups = await getMyGroups(userId)

return NextResponse.json(myGroups);
}