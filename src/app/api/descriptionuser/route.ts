import { NextResponse } from "next/server";
import  bcrypt  from 'bcrypt'

import prisma from "@/app/libs/prismadb";
import getCurrentUser from "@/app/actions/getCurrentUser";


export async function PUT(
    request: Request, 
    ) {

        const body = await request.json();

  const { 
    description,
    id
   } = body;


    const user = await prisma.user.update({
        where: {
            id
        },
        data: {
          description
        }
      });
    
  return NextResponse.json(user.description);
}