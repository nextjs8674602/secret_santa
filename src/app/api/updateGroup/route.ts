import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";

export async function PUT(
  request: Request, 
) {
  const body = await request.json()

  const {groupId, description, name, maximumPrice, initialData} = body
  
 
  const group = await prisma.group.update({
    where: {
        id: groupId
    },
    data: {
      description,
      name,
      priceMaximum: parseFloat(maximumPrice),
      initialData
    }
  });


  return NextResponse.json(group);
}