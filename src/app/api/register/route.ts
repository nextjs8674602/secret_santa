import { NextResponse } from "next/server";
import  bcrypt  from 'bcrypt'

import prisma from "@/app/libs/prismadb";
import { validateDateBirth } from "@/app/validations/formsValidations/registerValidate";
import { setFormattedDate } from "@/app/actions/getFormattedDate";

export async function POST(
  request: Request, 
) {
  const body = await request.json();
  let { 
    email,
    name,
    lastName,
    dateBirth,
    password
   } = body;

   const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
   if(!regexEmail.test(email)){
    return NextResponse.json({error: 'Por favor, insira um endereço de e-mail válido.'}, {status:400})
  }

  if(password.trim() === ""){
    return NextResponse.json({error: 'Por favor, insira uma senha válida.'}, {status:400})
  }

  if(password.length < 6 && password.length > 15){
    return NextResponse.json({error: 'Por favor, insira uma senha com no mínimo seis caracteres e no máximo quinze caracteres.'}, {status:400})
  }

  if(password.length < 6){
    return NextResponse.json({error: 'Por favor, insira uma senha com no mínimo seis caracteres.'}, {status:400})
  }



  if(!validateDateBirth(dateBirth)){
    return NextResponse.json({error: 'Essa data está incorreta, por favor altere para uma data válida.'}, {status:400})
  }

  const formattedDate = setFormattedDate(dateBirth)
  dateBirth = new Date(formattedDate)
  
   const emailValidation = await prisma.user.findFirst({
    where: {
      email
    }
  });
   
  if(emailValidation){
    return NextResponse.json({error: 'Email já cadastrado.'}, {status:400})
  }

   const newPassword = await bcrypt.hash(password, 12);

   const user = await prisma.user.create({
    data: {
      email:email.trim(),
      name: name,
      password: newPassword,
      lastName: lastName,
      dateBirth
    }
  });

  return NextResponse.json(user);
}