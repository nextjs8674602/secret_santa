import { NextResponse } from "next/server";

import prisma from "@/app/libs/prismadb";
import getMyGifts from "@/app/actions/getMyGifts";

export async function PUT(
  request: Request, 
) {
  const body = await request.json();

  const { 
    userId,
    notifications
   } = body;
 
   await prisma.user.update({
    where: {
        id:userId
    },
    data:{
        notifications
    }
  })


  return NextResponse.json({});
}