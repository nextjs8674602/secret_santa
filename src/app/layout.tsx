import type { Metadata } from 'next'
import './globals.css'
import Navbar from './components/Navbar'
import getCurrentUser from './actions/getCurrentUser'
import Modal from './components/Modal'
import ToasterProvider from './providers/ToasterProvider'
import SecretSantaLoading from './components/SecretSantaLoding'
import Loader from './components/Loader'


export const metadata: Metadata = {
  title: 'Amigo Secreto'
}

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
   
  const currentUser = await getCurrentUser()

  return (
    <html lang="en">
      <body className=" h-screen bg-gray-50">
      {/* {currentUser && <EditModal currentUser={currentUser}/>} */}
      {/* <Modal /> */}
      <ToasterProvider />
      <div>
      {children}
      </div>
      </body>
    </html>
  )
}
