'use client'

import { FieldValues, SubmitHandler, useForm } from "react-hook-form"
import Button from "../components/Button"
import Input from "../components/inputs/Input"
import axios from "axios"
import { useRouter } from "next/navigation"
import Image from 'next/image'
import { useState } from "react"
import SubtitleValidation from "../components/SubtitleValidation"
import { truncateSync } from "fs"
import { registerFormSchema } from "../validations/formsValidations/registerValidate"
import emailValidation from "../validations/emailValidate"
import gettingEmailValidation from "../validations/emailValidate"
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import SpinnerLoad from "../components/SpinnerLoad"
import toast from 'react-hot-toast'
import InputDate from "../components/inputs/InputDate"


const Register = () => {

    const route = useRouter()
    const [loading, setLoading]= useState(false)
    const [statusLogin, setStatusLogin]= useState({
      status: true,
      message: ""
    })

    const {
        register,
        handleSubmit,
        formState: {
            errors,
        }
    } = useForm<FieldValues>({
      resolver: zodResolver(registerFormSchema)
    })

    const onSubmit: SubmitHandler<FieldValues> = (data) => {
      
      
      setLoading(true)
      axios.post('/api/register', data)
      .then((dataRegister) => {
        setLoading(false)
        toast.success('Cadastro realizado com sucesso !')
        route.push("/")
      })
      .catch((error) => {
        setLoading(false)
        try{
          if(error.response.status === 400){
            const errorMessage = error.response.data.error
            const statusLoginUpdated = {...statusLogin}
  
            statusLoginUpdated.status = false
            statusLoginUpdated.message = errorMessage
            setStatusLogin(statusLoginUpdated)
          }
        }
        catch(e){
          const errorMessage = "Instabilidade no servidor. Por favor, tente mais tarde."
          const statusLoginUpdated = {...statusLogin}

          statusLoginUpdated.status = false
          statusLoginUpdated.message = errorMessage
          setStatusLogin(statusLoginUpdated)
        }
        
      })
      .finally(() => {
      
      })
  }

    return (
      <section className="flex min-h-screen bg-slate-50 overflow-y-auto">
        <div className="items-center h-screen hidden md:flex w-2/3 justify-center">
        <Image src="/logo.png" alt="Descrição da Imagem" width={300} height={100} />
        </div>
      <div className="flex flex-col justify-center overflow-y-auto overflow-scroll border-l-2 shadow-2xl w-dvw border-green-700 p-10  md:w-1/3">
        <h1 className="font-bold text-4xl text-center mb-10  text-green-700">Cadastro</h1>
        <div className="flex flex-col gap-2">
          <div>
          <Input status={errors.name} id="name" register={register} type="text" label="Nome"/>
         {errors.name && <SubtitleValidation subtitle={errors.name.message}/>} 
          </div>
          <div>
          <Input status={errors.lastName} id="lastName" register={register} type="text" label="Sobrenome"/>
          {errors.lastName && <SubtitleValidation subtitle={errors.lastName.message}/>}
          </div>
         <div>
         <InputDate status={errors.dateBirth} id="dateBirth" register={register} type="text" label="Data de Nascimento"/>
         {errors.dateBirth && <SubtitleValidation subtitle={errors.dateBirth.message}/>}
         </div>
          <div>
          <Input status={errors.email} id="email" register={register} type="email" label="Email"/>
          {errors.email && <SubtitleValidation subtitle={errors.email.message}/>}
          </div>
          <div>
          <Input status={errors.password} id="password" register={register} type="password" label="Senha"/>
          {errors.password && <SubtitleValidation subtitle={errors.password.message}/>}
          </div>
          <div>
          <Input status={errors.confirmPassword} id="confirmPassword" register={register} type="password" label="Confirmar Senha"/>
          {errors.confirmPassword && <SubtitleValidation subtitle={errors.confirmPassword.message}/>}
          </div>

          {!statusLogin.status && <span className="bg-red-100 mt-5 border text-center border-red-400 text-red-700 px-4 py-2 rounded-md">{statusLogin.message}</span>} 
          {loading ? 
           <SpinnerLoad />
           :
           <div className="text-center">
           <Button
           onSubmit={handleSubmit(onSubmit)} 
           name="Cadastrar"
           />
           </div>
          }
          

          
         
        </div>
      </div>
      </section>
      
    )
  }
  
  export default Register