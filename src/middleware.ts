'use client'


import { NextResponse } from 'next/server'
import { withAuth, NextRequestWithAuth, NextAuthMiddlewareOptions } from 'next-auth/middleware'

const middleware = (request: NextRequestWithAuth) => {

const currentUrl = new URL(request.url);

    if(!request.nextauth.token){
      return NextResponse.rewrite(new URL('/login', request.url))
    }
    else if(currentUrl.pathname === "/" && request.nextauth.token){
      return NextResponse.redirect(new URL(`/perfil/${request.nextauth.token.sub}`, request.url))
    }
   
  
}

const callbackOptions: NextAuthMiddlewareOptions = {}

export default withAuth(middleware, callbackOptions)

export const config = {
  matcher: ['/','/perfil/:path*', '/group/:path*', '/notifications']
}